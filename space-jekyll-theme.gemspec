# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name          = 'space-jekyll-theme'
  s.version       = '0.0.1'
  s.license       = 'MIT'
  s.authors       = ['Timothy Cyrus']
  s.email         = ['tcyrus@tcyr.us']
  s.homepage      = 'https://gitlab.com/tcyr.us/space-jekyll-theme'
  s.summary       = 'Custom Jekyll theme'

  s.files         = `git ls-files -z`.split("\x0").select do |f|
    f.match(%r{^((_includes|_layouts|_sass|assets)/|(LICENSE|README)((\.(txt|md|markdown)|$)))}i)
  end

  s.platform = Gem::Platform::RUBY

  s.add_runtime_dependency 'jekyll', '~> 4.3'
  s.add_runtime_dependency 'jekyll-feed', '~> 0.17.0'

  s.add_development_dependency 'bundler'
end
