(function($, window, undefined) {
    const fade = document.getElementById('fade'),
          aSlide = document.getElementById('slide'),
          sidebar = document.getElementById('sidebar');

    const open = document.getElementById('open'),
          search = document.getElementById('search'),
          close = document.getElementById('close');

    const searchForm = document.getElementById('search-form'),
          searchClose = document.getElementById('search-close'),
          searchField = document.getElementById('search-field'),
          searchWrapper = document.getElementById('search-wrapper');

    function toggleMenu(status) {
        sidebar.classList.toggle('slide', status);
        aSlide.classList.toggle('slide', status);
        fade.classList.toggle('slide', status);
        /*
        $(open).toggle(!status);
        $(search).toggle(!status);
        $(close).toggle(status);
        */
        open.style.display = !status ? '' : 'none';
        search.style.display = !status ? '' : 'none';
        close.style.display = status ? '' : 'none';
    }

    const openMenu = toggleMenu.bind(null, true),
          closeMenu = toggleMenu.bind(null, false);

    // Menu
    aSlide.addEventListener('click', openMenu);
    fade.addEventListener('click', closeMenu);

    // Remove space scroll
    window.onkeydown = function(e) {
        if (e.keyCode === 32 && e.target === document.body) {
            e.preventDefault();
            return false;
        }
    };

    // Search panel mouse click event support
    const searching = document.getElementById('searching');
    searching.addEventListener('click', function() {
        closeMenu();
        search.click();
    });

    // Keys
    document.addEventListener('keydown', function(e) {
        if (searchForm.classList.contains('active')) {
            if (e.key === 'Escape') {
                $(searchClose).click();
            }
        } else {
            switch (e.key) {
                case 'Escape':
                    closeMenu();
                    break;
                default:
                    let trigger = [];

                    if (sidebar.classList.contains('slide')) {
                        trigger = sidebar.querySelectorAll('ul [key-trigger="' + e.key + '"]');
                    }
                    if (trigger.length === 0) {
                        trigger = document.querySelectorAll('header > a[key-trigger="' + e.key + '"]');
                    }

                    trigger.forEach(function(el) {
                        el.click();
                    });

                    break;
            }
        }
    });

    // Key release
    // Fix: extra character 's' is entered to the input field when
    //      switching to the search form by using keypress
    document.addEventListener('keyup', function() {
        if (searchForm.classList.contains('active')) {
            searchField.focus();
        } else {
            searchField.blur();
        }
    });

    // Search
    search.addEventListener('click', function() {
        searchWrapper.classList.add('active');
        searchForm.classList.add('active');
        // Only focus the form after key release, prevent the character 's'
        // entered immediately after key enter
        document.body.classList.add('search-overlay');
        $(searchField).simpleJekyllSearch();
    });

    searchClose.addEventListener('click', function() {
        searchWrapper.classList.remove('active');
        searchForm.classList.remove('active');
        document.body.classList.remove('search-overlay');
    });

    // Kramdown Figure Hack
    // TODO: Replace with VanillaJs
    $('.figure').each(function() {
        $(this).wrap(document.createElement('figure'));
        $(this).after($('<figcaption>', { text: $(this).attr('title') }));
        $(this).removeAttr('title');
    });

})(Zepto, window);
