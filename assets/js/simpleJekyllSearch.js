(function($) {
    $.fn.simpleJekyllSearch = function(options) {
        const settings = Object.assign({
            jsonFile: '../../search.json',
            jsonFormat: 'title,category,desc,url,date,shortdate',
            template: '<li><article><a href="{url}">{title} <span class="entry-date"><time datetime="{date}">{date}</time></span></a></article></li>',
            searchResults: '.search-results',
            searchResultsTitle: '<h4>Search Results:</h4>',
            limit: 5,
            noResults: '<p>Oh snap!<br/><small>Nothing found! :(</small></p>'
        }, options);

        const properties = settings.jsonFormat.split(',');

        let jsonData = [],
            origThis = this,
            searchResultsEl = document.querySelector(settings.searchResults);

        if (settings.jsonFile.length && searchResultsEl !== null) {
            $.ajax({
                type: 'GET',
                url: settings.jsonFile,
                dataType: 'json',
                success: function(data, textStatus, jqXHR) {
                    jsonData = data;
                    registerEvent();
                },
                error: function(x, y, z) {
                    console.error(x.responseText, x, y, z);
                    // x.responseText should have what's wrong
                }
            });
        }


        function registerEvent() {
            origThis.keyup(function(e) {
                if ($(this).val().length) {
                    writeMatches( performSearch($(this).val()) );
                } else {
                    clearSearchResults();
                }
            });
        }

        function performSearch(str) {
            return jsonData.filter(function(entry) {
                return properties.some(function(property) {
                    return (entry[property] !== undefined && entry[property].toLowerCase().indexOf(str.toLowerCase()) !== -1);
                });
            });
        }

        function writeMatches(m) {
            clearSearchResults();

            searchResultsEl.insertAdjacentHTML("beforeend", settings.searchResultsTitle);

            if (m.length) {
                m.slice(0, settings.limit).forEach(function(entry) {
                    const template = settings.template;

                    const output = properties.reduce(function(out, property) {
                        let regex = new RegExp("\{" + property + "\}", 'g');
                        return out.replace(regex, entry[property]);
                    }, template);

                    searchResultsEl.insertAdjacentHTML("beforeend", output);
                });
            } else {
                searchResultsEl.insertAdjacentHTML("beforeend", settings.noResults);
            }
        }

        function clearSearchResults() {
            searchResultsEl.textContent = '';
        }
    }
}(Zepto));
